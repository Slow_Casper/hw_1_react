import { Component } from 'react';
import PropTypes from "prop-types"
import './Button.scss'

export default class Button extends Component {

    render() {
        const {backgroundColor, classes, text, onClick} = this.props;
        return (
            <button 
                style={{
                    backgroundImage: backgroundColor
                }}
                className={classes} 
                type='button'
                onClick={onClick}
            >
                {text}
            </button>
        )
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    classes: PropTypes.string,
    text: PropTypes.string,
    onclick: PropTypes.func
}

Button.defaultProps = {
    backgroundColor: "white",
    classes: "btn"
}