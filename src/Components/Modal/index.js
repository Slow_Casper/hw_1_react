import {Component} from 'react';
import PropTypes from "prop-types"
import "./Modal.scss"


export default class Modal extends Component {

    render() {
        const { text, closeModal } = this.props
        return (
            <div className='modal-background' onClick={this.close}>
                <div className='modal'>

                    <p className='modal-text'>{text}</p>
                    <button onClick={closeModal}>OK</button>

                </div>
            </div>
        )
    }

    close = (event) => {
        if(event.target === event.currentTarget) 
        
        this.props.closeModal()
    }

}

Modal.propTypes = {
    text: PropTypes.string,
    closeModal: PropTypes.func
}