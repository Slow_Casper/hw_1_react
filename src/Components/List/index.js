import { Component } from "react";
import PropTypes from "prop-types"
import "./List.scss"
import Card from "../Card";


export default class List extends Component {

    render() {
        const { store, cartAndWish } = this.props
        return (
            <section className="store">
                <div className="container">
                    <div className="store-list">
                        {store.map(( el ) => <Card key={el.id} cartAndWish={cartAndWish} {...store[el.id - 1]} />)}
                    </div>
                </div>
            </section>
        )
    }
}

List.propTypes = {
    store: PropTypes.array,
    cartAndWish: PropTypes.func,
}