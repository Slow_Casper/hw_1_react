import { Component } from "react";
import PropTypes from "prop-types"
import "./Card.scss"
import Button from "../Button"
import Modal from "../Modal";


export default class Card extends Component {

    state = {
        activeModal: false,
        favorite: false,
        storeCard: this.props,
    }

    componentDidMount() {
        if(localStorage.wishList.includes(this.state.storeCard.id)) {
            this.setState({favorite: true})
        } else {
            this.setState({favorite: false})
        }
    }
    
    closeModal = () => {
        this.setState(() => ({         
            activeModal: "",
        }));
    }
    
    addToCart(modal, id) {
        const { cartAndWish } = this.props
        this.setState((prev) => (prev.activeModal = modal));
        let cartList = localStorage.cartList ? JSON.parse(localStorage.cartList) : []
        if (cartList.includes(id)) {
            cartList = cartList.filter(item => item !== id)
        } else {
            cartList.push(id)
        }
        localStorage.cartList = JSON.stringify(cartList)
        cartAndWish()
    }

    addToWish(id) {
        const { cartAndWish } = this.props
        let wishList = localStorage.wishList ? JSON.parse(localStorage.wishList) : []
        if (wishList.includes(id)) {
            wishList = wishList.filter(item => item !== id)
            this.setState({
                favorite: false,
            })
        } else {
            wishList.push(id)
            this.setState({
                favorite: true,
            })
        }
        localStorage.wishList = JSON.stringify(wishList)
        cartAndWish()
    } 


    render() {
        const { id, name, price, url, width, height, article, color } = this.state.storeCard

        return (
            <>
                <div className="card" >
                    <img src={url} alt="#" width={width} height={height}></img>
                    <p className="card-article">{article}</p>
                    <h2 className="card-title">{name}</h2>
                    <div className="card-description">
                        <div className={color} style={{backgroundColor: color}}></div>
                        <p>{price}</p>
                    </div>
                    <div className="card-buttons">
                        <Button text={!JSON.parse(localStorage.cartList).includes(id) ? "Add to cart" : "Remove from card"} onClick={() => {
                            this.addToCart("true", id)
                        }} />
                        <button className="fav-btn" onClick={() => {
                                this.addToWish(id)
                            }}>
                            {this.state.favorite ? (<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="rgb(200, 150, 50)" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg>) : 
                            (<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 16 16">
                                <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"/>
                            </svg>)
                            }
                        </button>
                    </div>
                </div>
                {this.state.activeModal && (
                    <Modal 
                        text={JSON.parse(localStorage.cartList).includes(id) ? "Товар було додано до кошика!" : "Товар було видалено з кошика!"}
                        closeModal={() => this.closeModal()}
                    />
                )}
            </>
        )
    }
}

Card.propTypes = {
    id: PropTypes.number,
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    cartAndWish: PropTypes.func
}